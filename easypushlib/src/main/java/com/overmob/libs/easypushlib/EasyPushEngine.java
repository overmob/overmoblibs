package com.overmob.libs.easypushlib;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

/**
 * Created by burt on 13/02/15.
 */
public class EasyPushEngine {

    public static final String PROPERTY_REG_ID = "registration_id";
    public static final String PROPERTY_APP_VERSION = "appVersion";

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static EasyPushEngine _instance = null;
    /**
     * Substitute you own sender ID here. This is the project number you got
     * from the API Console, as described in "Getting Started."
     */

    private Context context;
    private PushEngineSettings engineSettings;

    private GoogleCloudMessaging gcm;

    private RegisterInBackgroundAsync registerInBackGroundAsynkTask = null;

    private EasyPushEngine(Context context) {
        this.context = context;
    }


    public static EasyPushEngine getInstance(Context context) {
        if (_instance == null) {
            _instance = new EasyPushEngine(context);
        }

        return _instance;
    }

    /**
     * @return Application's {@code SharedPreferences}.
     */
    public static SharedPreferences getGCMPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return context.getSharedPreferences(_instance.getClass().getSimpleName(), Context.MODE_PRIVATE);
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    public static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    public void init(String projectKey, String clientSecret) {
        init(new PushEngineSettings(projectKey, clientSecret));
    }

    public void init(PushEngineSettings engineSettings) {
        if (_instance.checkPlayServices(context)) {
            this.engineSettings = engineSettings;
            _instance.initGCM();
        }
    }

    public PushEngineSettings getSettings() {
        return engineSettings;
    }

    public void setSettings(PushEngineSettings engineSettings) {
        this.engineSettings = engineSettings;
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices(Context activity) {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, (Activity) activity, PLAY_SERVICES_RESOLUTION_REQUEST).show();
                L.w("GooglePlayServices NOT Available");
            } else {
                L.e("This device is not supported.");
                Toast.makeText(activity, activity.getString(R.string.device_not_supported), Toast.LENGTH_LONG);
                //activity.finish();
            }
            return false;
        }
        return true;
    }

    private void initGCM() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        if (resultCode == ConnectionResult.SUCCESS) {
            gcm = GoogleCloudMessaging.getInstance(context);
            String registrationId = getRegistrationId(context);

            if (registrationId.isEmpty()) {
                if (registerInBackGroundAsynkTask == null || registerInBackGroundAsynkTask.getStatus() == AsyncTask.Status.FINISHED) {
                    registerInBackGroundAsynkTask = new RegisterInBackgroundAsync(context, engineSettings);
                    registerInBackGroundAsynkTask.execute();
                } else {
                    L.w("registerInBackGroundAsynkTask already in execution...");
                }
            }
        } else {
            L.w("GooglePlayServicesUtil not availbale. Try with checkPlayServices(Activity activity)");
        }
    }

    /**
     * Gets the current registration ID for application on GCM service.
     * <p/>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     * registration ID.
     */
    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            L.i("GCM Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            L.i("GCM App version changed.");
            return "";
        }
        L.i("GCM Registration Id: " + registrationId);
        return registrationId;
    }


}
