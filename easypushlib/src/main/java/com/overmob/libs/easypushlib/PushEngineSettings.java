package com.overmob.libs.easypushlib;

/**
 * Created by burt on 03/03/15.
 */
public class PushEngineSettings {

    public String BASE_URL = "http://push.overmob.com";
    public String WS_URL_DEVICE_REGISTRATION = "/deviceRegistration";
    public String SENDER_ID = "659555516582";

    public String projectKey;
    public String clientSecret;

    public PushEngineSettings(String projectKey, String clientSecret) {
        this.projectKey = projectKey;
        this.clientSecret = clientSecret;
    }


}
