package com.overmob.libs.easypushlib;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by burt on 13/02/15.
 */
public class RegisterInBackgroundAsync extends AsyncTask<Void, Void, String> {

    private final PushEngineSettings engineSettings;
    private Context context;
    private GoogleCloudMessaging gcm;

    public RegisterInBackgroundAsync(Context context, PushEngineSettings engineSettings) {
        this.context = context;
        this.engineSettings = engineSettings;
    }

    @Override
    protected String doInBackground(Void... params) {
        String msg = "";
        try {
            if (gcm == null) {
                gcm = GoogleCloudMessaging.getInstance(context);
            }
            String regid = gcm.register(engineSettings.SENDER_ID);
            msg = "Device registered, registration ID=" + regid;

            // You should send the registration ID to your server over HTTP,
            // so it can use GCM/HTTP or CCS to send messages to your app.
            // The request to your server should be authenticated if your app
            // is using accounts.
            //sendRegistrationIdToBackend(regid);
            postRegistrationIdToBackend(regid);


        } catch (IOException ex) {
            msg = "Error RegisterInBackgroundAsync:" + ex.getMessage();
            // If there is an error, don't just keep trying to register.
            // Require the user to click a button again, or perform
            // exponential back-off.
        }
        return msg;
    }

    @Override
    protected void onPostExecute(String msg) {
        L.i(msg);
    }

    /**
     * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP
     * or CCS to send messages to your app. Not needed for this demo since the
     * device sends upstream messages to a server that echoes back the message
     * using the 'from' address in the message.
     *
     * @param regid
     */
    private void sendRegistrationIdToBackend(String regid) throws IOException {
        URL url = null;
        HttpURLConnection urlConnection = null;
        try {
            url = new URL(engineSettings.BASE_URL + "?page=register&action=register&devicemodel=TEST_EASYPUSH&version=2.1&deviceid=" + regid + "");
            L.i("Send regId to backend");
            try {
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                if (!url.getHost().equals(urlConnection.getURL().getHost())) {
                    // we were redirected! Kick the user out to the browser to sign on?
                    throw new IOException("Impossibile contattare il server " + url.getHost());
                } else {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                    StringBuilder result = new StringBuilder();
                    String line;
                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }
                    JSONObject jsonRes = new JSONObject(result.toString());
                    if (jsonRes.getBoolean("status")) {
                        L.i("Backend saved regId");
                        // Persist the regID - no need to register again.
                        storeRegistrationId(regid);
                    } else {
                        throw new IOException("RegId non salvato su server remoto, risponsta: " + jsonRes);
                    }
                }
            } catch (IOException e) {
                throw new IOException("Backend error: " + e.getMessage());
            } catch (JSONException ej) {
                throw new IOException("Risposta del server non valida " + ej.getMessage());
            } finally {
                if (urlConnection != null) urlConnection.disconnect();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

    }

    /**
     * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP
     * or CCS to send messages to your app. Not needed for this demo since the
     * device sends upstream messages to a server that echoes back the message
     * using the 'from' address in the message.
     *
     * @param regid
     */
    public void postRegistrationIdToBackend(String regid) throws IOException {
        // Create a new HttpClient and Post Header
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(engineSettings.BASE_URL + engineSettings.WS_URL_DEVICE_REGISTRATION);
        String responseString = "";
        try {
            httppost.addHeader("ProjectKey", engineSettings.projectKey);
            httppost.addHeader("Authorization", engineSettings.clientSecret);

            // Add your data
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("action", "register"));
            nameValuePairs.add(new BasicNameValuePair("deviceid", regid));
            //nameValuePairs.add(new BasicNameValuePair("username", "burt"));
            nameValuePairs.add(new BasicNameValuePair("osTag", "Android"));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            // Execute HTTP Post Request
            HttpResponse response = httpclient.execute(httppost);
            responseString = EntityUtils.toString(response.getEntity());
            JSONObject jsonRes = new JSONObject(responseString);
            if (jsonRes.getBoolean("res")) {
                L.i("Backend saved regId");
                // Persist the regID - no need to register again.
                storeRegistrationId(regid);
            } else {
                throw new IOException("RegId non salvato su server remoto, risponsta: " + jsonRes);
            }
        } catch (IOException e) {
            throw new IOException("Backend error: " + e.getMessage());
        } catch (JSONException ej) {
            throw new IOException("Risposta del server non valida " + responseString);
        }
    }

    /**
     * Stores the registration ID and app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param regId registration ID
     */
    private void storeRegistrationId(String regId) {
        final SharedPreferences prefs = EasyPushEngine.getGCMPreferences(context);
        int appVersion = EasyPushEngine.getAppVersion(context);
        L.i("Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(EasyPushEngine.PROPERTY_REG_ID, regId);
        editor.putInt(EasyPushEngine.PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }
}
